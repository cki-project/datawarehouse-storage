"""Tests for the minio interface."""
import io
import os
import unittest

from minio import Minio


class TestConnection(unittest.TestCase):
    """Test the minio interface."""

    def setUp(self):
        """Prepare the minio test harness."""
        self.client = Minio(os.getenv('MINIO_SERVER', 'localhost:9000'),
                            access_key=os.getenv('MINIO_ACCESS_KEY'),
                            secret_key=os.getenv('MINIO_SECRET_KEY'),
                            secure=False)

        self._clear_harness()
        self.client.make_bucket('test-bucket')

    def tearDown(self):
        """Remove the minio test harness."""
        self._clear_harness()

    def _clear_harness(self):
        try:
            for obj in self.client.list_objects('test-bucket', recursive=True):
                self.client.remove_object(obj.bucket_name, obj.object_name)
            self.client.remove_bucket('test-bucket')
        except Exception:  # pylint: disable=broad-except
            pass

    def test_connection(self):
        """Smoke test."""
        data = b'ABCDEF'
        self.client.put_object('test-bucket', 'test-obj', io.BytesIO(b'ABCDEF'), len(data))
        self.assertEqual(self.client.get_object('test-bucket', 'test-obj').read(), data)
