FROM registry.gitlab.com/cki-project/containers/python:latest
ENV REQUESTS_CA_BUNDLE /etc/ssl/certs/ca-bundle.crt
ENV PYTHONUNBUFFERED 1

RUN mkdir -p /code/datawarehouse-storage
COPY . /code/datawarehouse-storage/

WORKDIR /code/datawarehouse-storage
RUN pip install -e .

ENTRYPOINT ["/bin/sleep", "infinity"]
