# Development

- build the container with `buildah bud -t datawarehouse-storage .`
- start it up with `podman play kube datawarehouse-storage.yaml`

The source code is bind-mounted directly into the `storage-test` container.
